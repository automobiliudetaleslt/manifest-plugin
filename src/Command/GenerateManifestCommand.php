<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Command;

use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitInterface;
use Sylius\Component\Shipping\Model\ShipmentInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGeneratorInterface;
use Omni\Sylius\ManifestPlugin\Model\Manifest;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \DateTime;

class GenerateManifestCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('shipment:manifest:generate')
            ->setDescription('Generates a manifest file for a specified shipping method.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $businessUnits = $this->getBusinessUnits();
        $providerPicker = $this->getContainer()->get(
            'omni_sylius_manifest.generator.manifest_generation_provider_picker'
        );

        $current = new \DateTime();

        foreach ($businessUnits as $businessUnit) {
            foreach ($providerPicker->getProvidersForManifestGeneration($businessUnit) as $provider) {
                $generated = $this->generateManifest($businessUnit, $provider['code'], $provider['manifestTime']);
                $output->writeln(sprintf('[%s] %s generated %s', $businessUnit->getErpCode(), $provider, $generated));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function generateManifest(
        BusinessUnitInterface $businessUnit,
        string $gatewayCode,
        DateTime $timeShouldBeClosed
    ) {
        $gateway = $this->findShippingGateway($gatewayCode);
        $exports = $this->getShipmentExports($businessUnit, $gatewayCode, $timeShouldBeClosed);

        if (count($exports) > 0) {
            /** @var Manifest $manifest */
            $manifest = $this->getManifestFactory()->createNew();

            $manifest->setShippingGateway($gateway);
            $manifest->setShippingExports($exports);
            $manifest->setBusinessUnit($businessUnit);

            $path = $this->getManifestGeneratorManager()->generate($manifest);
            $manifest->setPath($path);
            $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
            $em->persist($manifest);
            $em->flush();
        }

        return count($exports);
    }

    /**
     * @param BusinessUnitInterface $businessUnit
     * @param string $gatewayCode
     * @param DateTime $timeShouldBeClosed
     * @return array
     */
    protected function getShipmentExports(
        BusinessUnitInterface $businessUnit,
        string $gatewayCode,
        DateTime $timeShouldBeClosed
    ): array {
        $states = $this->getContainer()->getParameter('shipment_ready_for_courier_states');
        $shipments = $this->getContainer()->get('sylius.repository.shipment')
            ->findShipmentForManifest($states, $businessUnit, $timeShouldBeClosed);
        $gatewayRepo = $this->getContainer()->get('bitbag.repository.shipping_gateway');

        return array_reduce(
            $shipments,
            function (array $carry, ShipmentInterface $shipment) use ($states, $gatewayCode, $gatewayRepo) {
                $gateway = $gatewayRepo->findOneByShippingMethod($shipment->getMethod());
                if (!$gateway instanceof ShippingGatewayInterface
                    || strpos($gateway->getCode(), $gatewayCode) === false
                ) {
                    return $carry;
                }

                $export = $shipment->getShippingExport();
                $manifest = $this->getManifestRepository()->findManifestByExport($export);
                if (!$manifest instanceof Manifest) {
                    array_push($carry, $export);
                }

                return $carry;
            },
            []
        );
    }

    /**
     * @return BusinessUnitInterface[]
     */
    protected function getBusinessUnits(): array
    {
        return $this->getContainer()->get('omni_sylius.repository.business_unit')->findAll();
    }

    /**
     * @param string $code
     * @return ShippingGatewayInterface|null
     */
    protected function findShippingGateway(string $code): ?ShippingGatewayInterface
    {
        return $this->getContainer()->get('bitbag.repository.shipping_gateway')->findOneByCode($code);
    }

    /**
     * @return object|\Omni\Sylius\ManifestPlugin\Manager\ManifestGeneratorManager
     */
    protected function getManifestGenerator(): ManifestGeneratorInterface
    {
        return $this->getContainer()->get('omni_sylius_manifest.generator_manager.manifest');
    }

    /**
     * @return FactoryInterface
     */
    protected function getManifestFactory(): FactoryInterface
    {
        return $this->getContainer()->get('omni_sylius_manifest.factory.manifest');
    }
}
