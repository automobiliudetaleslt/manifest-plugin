<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Generator;

use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitInterface;

interface GenerationTimeCheckerInterface
{
    /**
     * Returns true if its time to generate a manifest for a given business unit
     *
     * @param BusinessUnitInterface $businessUnit
     *
     * @return bool
     */
    public function isGenerationDue(BusinessUnitInterface $businessUnit): bool;

    /**
     * Returns providers manifest time  if its set. Otherwise returns null
     *
     * @param BusinessUnitInterface $businessUnit
     *
     * @return \DateTime|null
     */
    public function getManifestTime(BusinessUnitInterface $businessUnit): ?\DateTime;

    /**
     * The code of the provider like lpe, dpd, omniva, etc.
     *
     * @return string
     */
    public function getProviderCode(): string;
}
